
use lambda_http::{run, service_fn, Request, Response, Body, Error};
use mysql_async::{Pool, prelude::Queryable};
use std::env;


async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
      // 从环境变量中获取数据库连接信息
    //   let host = env::var("DB_HOST").expect("DB_HOST environment variable not set");
    //   let port = env::var("DB_PORT").expect("DB_PORT environment variable not set");
    //   let db_name = env::var("DB_NAME").expect("DB_NAME environment variable not set");
    //   let user = env::var("DB_USER").expect("DB_USER environment variable not set");
    //   let password = env::var("DB_PASSWORD").expect("DB_PASSWORD environment variable not set");
  
      // 构建数据库连接URL
      let database_url = "mysql://admin:Lmt123456@myrustdb.cex915ik2op0.us-east-1.rds.amazonaws.com:3306/myrustdb";
  
      // 创建数据库连接池
      let pool = Pool::new(database_url);

      let mut conn = pool.get_conn().await.expect("Failed to connect to database");
  

      let row: Option<(i32,)> = conn.query_first("SELECT COUNT(*) FROM my_table").await.expect("Failed to execute query");

      // 构建响应
      let res_body = match row {
          Some((result,)) => format!("Query result: {}", result),
          None => "No result found".to_string(),
      };
  
      let response = Response::builder()
          .status(200)
          .header("Content-Type", "text/plain")
          .body(Body::from(res_body))
          .expect("Failed to build response");
  
      Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await
}

