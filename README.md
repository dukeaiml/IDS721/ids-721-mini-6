# IDS - Mini 5

### Overview 
Serverless Rust Microservice 

Create a Rust AWS Lambda function

Implement a simple service: Connect to AWS RDS mysql to query all the data 

### AWS RDS 

Create and set up a database called murustdb
![Alt text](<Screen Shot 2024-02-28 at 12.59.16.png>)


### AWS Lambda
To create a rust lambda function : 
`cargo lambda new <function name>`

To test the function

`cargo lambda watch`

`cargo lambda invoke <function name> --data-example apigw-request`

To build&deploy

`cargo lambda build --release && cargo lambda deploy`

![Alt text](<Screen Shot 2024-02-28 at 13.05.10.png>)

### Set up roles and permission

In order to get access to the RDS, we have to make the function running in the same VPC as the database. Also, we need to set up the role permissions for the function execution. 

1. Set up the lambda function for the database

![Alt text](<Screen Shot 2024-02-28 at 13.11.04.png>)

2. get the role permission

![Alt text](<Screen Shot 2024-02-28 at 13.10.19.png>)

### STORE DATA 
Run 

```bash
./CreateTable.sh
```

to upload the local csv file to the database.

### QUERY DATA 

![Alt text](<Screen Shot 2024-02-28 at 13.13.27.png>)


### Result


![Alt text](image.png)